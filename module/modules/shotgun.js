const axios = require('axios');
const config = require('../../config')

module.exports = {
  all: function (bot, msg, match) {
    axios.get(config.BASEURL+':'+config.PORT+'/api/Shotgun')
    .then(response => {
      message = "DE OFFICIËLE REGELS VAN SHOTGUN!\n\n";
      for (i = 0; i < response.data.length; i++) { 
        message += response.data[i].number + ": " +response.data[i].rule + "\n\n";
      }

      bot.sendMessage(msg.chat.id, message.toString());
    })
    .catch(err => {
      console.log(err);
    });
  },

  single: function (bot, msg, match){
    axios.get(config.BASEURL+':'+config.PORT+'/api/Shotgun?filter={"where":{"number":'+match[1]+'}}')
    .then(response => {
      if(response.data.length == 0){
        axios.get(config.BASEURL+':'+config.PORT+'/api/Shotgun/count')
        .then(response => {
          count = response.data.count;
          bot.sendMessage(msg.chat.id, "Number is out of range. Please use a number between 1 and "+ count);
        })
        .catch(err => {
          bot.sendMessage(msg.chat.id, "Something went wrong! Please try again.");
        });
      } else{
        message = "DE OFFICIËLE REGELS VAN SHOTGUN!\n\n";
        message += response.data[0].number + ": " +response.data[0].rule;
        bot.sendMessage(msg.chat.id, message.toString());
      }
    })
    .catch(err => {
      bot.sendMessage(msg.chat.id, "Something went wrong! Please try again.");
    });
  }
}