var help = require('./modules/help');
var yomama = require('./modules/yomama');
var kortebroek = require('./modules/kortebroek')
var yesno = require('./modules/yesno')
var thuis = require('./modules/thuis')
var image = require('./modules/image')
var shotgun = require('./modules/shotgun')
var buienradar = require('./modules/buienradar')

module.exports = {
  Help: help,
  Yomama: yomama,
  Kortebroek: kortebroek,
  Yesno: yesno,
  Thuis: thuis,
  Image: image,
  Shotgun: shotgun,
  Buienradar: buienradar
}